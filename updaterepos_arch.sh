#!/bin/bash

export GPG_TTY=$(tty)

sudo -u makepkg echo "$KEY" > /home/makepkg/priv.gpg
sudo -u makepkg gpg --import pub.gpg && sudo -u makepkg gpg --batch --import --batch /home/makepkg/priv.gpg


mkdir -p pkgs/arch


git clone https://codeberg.org/julianfairfax/repackage

chmod +x repackage/build_arch.sh
repackage/build_arch.sh

chown -R makepkg repackage

cd repackage/repackage
sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../pkgs/arch/

cd ../../

git clone https://codeberg.org/julianfairfax/spoofer

chmod +x spoofer/build_arch.sh
spoofer/build_arch.sh

chown -R makepkg spoofer

cd spoofer/spoofer

sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../pkgs/arch/

cd ../../


chmod +x gnome-dav-proxy/build_arch.sh
gnome-dav-proxy/build_arch.sh

chown -R makepkg gnome-dav-proxy

cd gnome-dav-proxy/gnome-dav-proxy
sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../pkgs/arch/

cd ../../


echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --batch --import priv.gpg

repo-add pkgs/arch/package-repo.db.tar.gz pkgs/arch/*.pkg.tar.zst -s
