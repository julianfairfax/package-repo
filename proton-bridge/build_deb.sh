#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="proton-bridge"
version="$(wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/version.txt -O -)"
description="Proton Mail Bridge application"

declare -a arches=("amd64" "arm64")

for arch in ${arches[@]}; do
	install -d "$name"/usr/bin

	wget -q https://julianfairfax.codeberg.page/proton-bridge-prebuilt/"$arch"/proton-bridge -O "$name"/usr/bin/"$name"

	wget -q https://julianfairfax.codeberg.page/proton-bridge-prebuilt/"$arch"/bridge -O "$name"/usr/bin/bridge

	chmod +x "$name"/usr/bin/"$name"

	chmod +x "$name"/usr/bin/bridge

	install -d "$name"/usr/lib/systemd/user

	echo "[Unit]
Description=Proton Mail Bridge application

[Service]
Type=simple
ExecStart=/usr/bin/$name -n

[Install]
WantedBy=default.target" | tee "$name"/usr/lib/systemd/user/"$name".service

	mkdir "$name"/DEBIAN

	echo "Package: $name
Version: $(echo $version | sed 's/v//' | sed 's/-//g')
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee "$name"/DEBIAN/control

	dpkg-deb -Z xz -b "$name"/ .

	cp *.deb ../

	rm -rf "$name"/DEBIAN
done
