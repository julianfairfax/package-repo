#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="gnome-dav-proxy"
version="$(wget -q -O - https://gitlab.com/api/v4/projects/julianfairfax%2Fgnome-dav-proxy/releases/permalink/latest | sed 's/.*"tag_name":"//' | sed 's/",".*//')"
description="GNOME CalDAV and CardDAV Proxy"

declare -a arches=("all")

wget -q https://gitlab.com/julianfairfax/gnome-dav-proxy/-/archive/$version/"$name"-$version.tar.gz

install -d "$name"/opt/"$name"

tar -xf "$name"-$version.tar.gz -C "$name"/opt/

cp "$name"/opt/"$name"-$version/"$name".py "$name"/opt"/$name"/

rm -rf "$name"/opt/"$name"-$version

chmod +x "$name"/opt/"$name"/"$name".py

install -d "$name"/usr/lib/systemd/user

echo "[Unit]
Description=GNOME CalDAV and CardDAV Proxy

[Service]
Type=simple
ExecStart=python3 /opt/$name/$name.py --config_file /home/%i/.config/$name.config

[Install]
WantedBy=default.target" | tee "$name"/usr/lib/systemd/user/"$name"@.service

for arch in ${arches[@]}; do
	mkdir "$name"/DEBIAN

	echo "Package: $name
Version: $(echo $version | sed 's/-//g')
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee "$name"/DEBIAN/control

	dpkg-deb -Z xz -b "$name"/ .

	cp *.deb ../

	rm -rf "$name"/DEBIAN
done