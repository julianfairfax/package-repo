#!/bin/bash

cd "${0%/*}"

name="gnome-dav-proxy"
version="$(wget -q -O - https://gitlab.com/api/v4/projects/julianfairfax%2Fgnome-dav-proxy/releases/permalink/latest | sed 's/.*"tag_name":"//' | sed 's/",".*//')"
description="GNOME CalDAV and CardDAV Proxy"

mkdir "$name"

echo "pkgname=\"$name\"
pkgver=\"$(echo $version | sed 's/-//g')\"
arch=(\"any\")
pkgdesc=\""$description"\"
pkgrel=\"1\"
source=(\"https://gitlab.com/julianfairfax/\$pkgname/-/archive/$version/\$pkgname-$version.tar.gz\")
sha256sums=(\"SKIP\")

package() {
	install -d \"\${pkgdir}\"/opt/\$pkgname

	cp \"\${srcdir}\"/\$pkgname-$version/\$pkgname.py \"\${pkgdir}\"/opt/\$pkgname/

	chmod +x \"\${pkgdir}\"/opt/\$pkgname/\$pkgname.py

	install -d \"\${pkgdir}\"/usr/lib/systemd/user

	echo \"[Unit]
Description=GNOME CalDAV and CardDAV Proxy

[Service]
Type=simple
ExecStart=python3 /opt/\$pkgname/\$pkgname.py --config_file /home/%i/.config/\$pkgname.config

[Install]
WantedBy=default.target\" | tee \"\${pkgdir}\"/usr/lib/systemd/user/\$pkgname@.service
}" | tee "$name"/PKGBUILD