[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Package Repo
Repository for installing my projects and others more easily.

## Updates
This repository is updated every day at 00:00 UTC+2. If updates are available at this time, you will receive them after the building process is completed.

## Packages
The following projects made by others are available as packages from this repo:
- [adw-gtk3](https://github.com/lassekongo83/adw-gtk3) as a Debian package, [AUR package](https://aur.archlinux.org/packages/adw-gtk3)
- [morewaita](https://github.com/somepaulo/MoreWaita) as a Debian package, [AUR package](https://aur.archlinux.org/packages/morewaita)
- [proton-bridge](https://github.com/ProtonMail/proton-bridge) as a Debian package
- [proton-ie](https://github.com/ProtonMail/proton-bridge/tree/ie-1.3.3) as a Debian package
- [proton-mail-export](https://github.com/ProtonMail/proton-mail-export) as a (amd64) Debian package
- [sbctl](https://github.com/Foxboron/sbctl) as a (amd64) Debian package, [Arch package](https://archlinux.org/packages/extra/x86_64/sbctl/)

- catalina-unus, [GitLab repo](https://gitlab.com/julianfairfax/catalina-unus), [Codeberg repo](https://codeberg.org/julianfairfax/catalina-unus) as a Homebrew package
- gnome-dav-proxy, [GitLab repo](https://gitlab.com/julianfairfax/gnome-dav-proxy), [Codeberg repo](https://codeberg.org/julianfairfax/gnome-dav-proxy) as an Arch and Debian package
- macos-downloader, [GitLab repo](https://gitlab.com/julianfairfax/macos-downloader), [Codeberg repo](https://codeberg.org/julianfairfax/macos-downloader) as a Homebrew package
- macos-patcher, [GitLab repo](https://gitlab.com/julianfairfax/macos-patcher), [Codeberg repo](https://codeberg.org/julianfairfax/macos-patcher) as a Homebrew package
- openinstallmedia, [GitLab repo](https://gitlab.com/julianfairfax/openinstallmedia), [Codeberg repo](https://codeberg.org/julianfairfax/openinstallmedia) as a Homebrew package
- osx-patcher, [GitLab repo](https://gitlab.com/julianfairfax/osx-patcher), [Codeberg repo](https://codeberg.org/julianfairfax/osx-patcher) as a Homebrew package
- repackage, [GitLab repo](https://gitlab.com/julianfairfax/repackage), [Codeberg repo](https://codeberg.org/julianfairfax/repackage) as an Arch, Debian, and RPM package
- spoofer, [GitLab repo](https://gitlab.com/julianfairfax/spoofer), [Codeberg repo](https://codeberg.org/julianfairfax/spoofer) as an Arch, Debian, RPM, and Homebrew package

## How to use
Follow the instructions for your operating system.

### How to add repository for Arch-based Linux distributions
Copy and run this code to add the repository:

```
echo "[package-repo]
SigLevel = Optional TrustAll
Server = https://julianfairfax.codeberg.page/package-repo/arch/" | sudo tee -a /etc/pacman.conf

curl -s https://julianfairfax.codeberg.page/package-repo/pub.gpg | sudo pacman-key --add -
sudo pacman-key --lsign-key C123CB2B21B9F68C80A03AE005B2039A85E7C70A
```

### How to add repository for Debian-based Linux distributions
Copy and run this code to add the repository:

```
curl -s https://julianfairfax.codeberg.page/package-repo/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/julians-package-repo.gpg

echo 'deb [ signed-by=/usr/share/keyrings/julians-package-repo.gpg ] https://julianfairfax.codeberg.page/package-repo/debs packages main' | sudo tee /etc/apt/sources.list.d/julians-package-repo.list
```

### How to add repository for Homebrew
Copy and run this code to add the repository:

```
brew tap julianfairfax/package-repo https://gitlab.com/julianfairfax/package-repo
```

### How to add repository for RPM-based Linux distributions
Copy and run this code to add the repository:

```
echo "[gitlab.com.julianfairfax_package_repo]
name=gitlab.com_julianfairfax_package_repo
baseurl=https://julianfairfax.codeberg.page/package-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://julianfairfax.codeberg.page/package-repo/pub.gpg
metadata_expire=1h" | sudo tee -a /etc/yum.repos.d/julians-package-repo.repo
```

## Licensing information
The packages hosted on this repository are under the licenses of their developers. This repository's license only applies to the code for the repository itself.

If you believe your code has been unjustly used or has been used without proper credit in this repository, please [contact](https://julianfairfax.codeberg.page/contact.html) me.

## Self-hosting information
If you are going to host a repository like this yourself, it is requested that you remove my content from it and replace it with you own. This includes references to my name, projects, or website.

This repository signs the packages it hosts using a GPG key. You must create a GPG key and set a `KEY` GitLab variable, GitHub secret, or Codeberg secret, containing your private key file, encoded in ASCII.

Your private key must not have a password set, otherwise this repository will not be able to use it. If you would like to have a password set on your private key, you will have to modify this repository's code to add support for this.

You must also replace the `pub.gpg` file with a copy of the public key that you can export from your private key.

You must modify this README to replace references to my name, projects, and website with your own.

Under the terms of the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/) license used by this project, you are required to use this same license in any and all redistributions of this project's code.

This repository one is made to be compatible with GitHub Pages, GitLab Pages, and Codeberg Pages.
