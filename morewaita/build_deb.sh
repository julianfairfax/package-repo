#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="morewaita"
version="$(wget https://api.github.com/repos/somepaulo/MoreWaita/releases/latest -O - -q | grep tag_name | sed 's/.* "//' | sed 's/".*//')"
description="An Adwaita style extra icons theme for Gnome Shell."

declare -a arches=("all")

wget -q https://github.com/somepaulo/MoreWaita/archive/refs/tags/$version.tar.gz

install -d "$name"/usr/share/icons

tar -xf $version.tar.gz -C "$name"/usr/share/icons/

mv "$name"/usr/share/icons/MoreWaita-$(echo $version | sed 's/v//') "$name"/usr/share/icons/MoreWaita

for arch in ${arches[@]}; do
	mkdir "$name"/DEBIAN

	echo "Package: $name
Version: $(echo $version | sed 's/v//')
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee "$name"/DEBIAN/control

	dpkg-deb -Z xz -b "$name"/ .

	cp *.deb ../

	rm -rf "$name"/DEBIAN
done
