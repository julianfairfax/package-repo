#!/bin/bash

echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --import priv.gpg

mkdir -p pkgs/rpms

mkdir pkgs/debs 


git clone https://codeberg.org/julianfairfax/repackage

chmod +x repackage/build_deb.sh
repackage/build_deb.sh

cp repackage/*.deb /tmp

chmod +x repackage/build_rpm.sh
repackage/build_rpm.sh

git clone https://codeberg.org/julianfairfax/spoofer

chmod +x spoofer/build_deb.sh
spoofer/build_deb.sh

cp spoofer/*.deb /tmp

chmod +x spoofer/build_rpm.sh
spoofer/build_rpm.sh


chmod +x adw-gtk3/build_deb.sh
adw-gtk3/build_deb.sh

cp adw-gtk3/*.deb /tmp

chmod +x gnome-dav-proxy/build_deb.sh
gnome-dav-proxy/build_deb.sh

cp gnome-dav-proxy/*.deb /tmp

chmod +x morewaita/build_deb.sh
morewaita/build_deb.sh

cp morewaita/*.deb /tmp

chmod +x proton-bridge/build_deb.sh
proton-bridge/build_deb.sh

cp proton-bridge/*.deb /tmp

chmod +x proton-ie/build_deb.sh
proton-ie/build_deb.sh

cp proton-ie/*.deb /tmp

chmod +x proton-mail-export/build_deb.sh
proton-mail-export/build_deb.sh

cp proton-mail-export/*.deb /tmp

chmod +x sbctl/build_deb.sh
sbctl/build_deb.sh

cp sbctl/*.deb /tmp


cp /root/rpmbuild/RPMS/noarch/*.rpm pkgs/rpms

mkdir -p pkgs/debs/conf
touch pkgs/debs/conf/option
echo "Codename: packages
Components: main
Architectures: amd64 i386 arm64 armhf
SignWith: yes" | tee pkgs/debs/conf/distributions

expect -c "spawn gpg --edit-key C123CB2B21B9F68C80A03AE005B2039A85E7C70A trust quit; send \"5\ry\r\"; expect eof"
rpm --define "_gpg_name Julian Fairfax <juliannfairfax@protonmail.com>" --addsign pkgs/rpms/*rpm

createrepo_c --database --compatibility pkgs/rpms/
gpg --local-user "Julian Fairfax <juliannfairfax@protonmail.com>" --yes --detach-sign --armor pkgs/rpms/repodata/repomd.xml

reprepro -V -b pkgs/debs includedeb packages /tmp/*deb
