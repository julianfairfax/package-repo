class Spoofer < Formula
    desc "Command line tool to spoof your MAC address and scan your network"
    homepage "https://gitlab.com/julianfairfax/spoofer"
    url "https://gitlab.com/julianfairfax/spoofer/-/archive/2.4.7/spoofer-2.4.7.tar.gz"
    sha256 "93375c533d635840deee1e1103efbd8a5fba607cb858763b8795a7f7b5d2769c"
    license "GPL"
    version "2.4.7"
  
    def install
        bin.install "spoofer.sh" => "spoofer"
        prefix.install Dir["resources/*"]
    end
end