class OsxPatcher < Formula
    desc "macOS command line tool for running OS X on unsupported Macs"
    homepage "https://gitlab.com/julianfairfax/osx-patcher"
    url "https://gitlab.com/julianfairfax/osx-patcher/-/archive/1.3/osx-patcher-1.3.tar.gz"
    sha256 "66726c8e12d049b3ca0c85b6635f0243a9dadef1b037f773d44be5c02e1df71d"
    license "GPL"
    version "1.3"
  
    def install
        bin.install "OS X Patcher.sh" => "osx-patcher"
        prefix.install Dir["resources/*"]
    end
end
