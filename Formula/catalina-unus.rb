class CatalinaUnus < Formula
    desc "macOS command line tool for running macOS Catalina on one HFS or APFS volume"
    homepage "https://gitlab.com/julianfairfax/catalina-unus"
    url "https://gitlab.com/julianfairfax/catalina-unus/-/archive/1.2.1/catalina-unus-1.2.1.tar.gz"
    sha256 "402cb72774047dae732f76f859f2421278cf6bfc1dc54e6d64a4032f92eecf2b"
    license "GPL"
    version "1.2.1"
  
    def install
        bin.install "Catalina Unus.sh" => "catalina-unus"
        prefix.install Dir["resources/*"]
    end
end