class Openinstallmedia < Formula
    desc "The open source install media creation tool for macOS"
    homepage "https://gitlab.com/julianfairfax/openinstallmedia"
    url "https://gitlab.com/julianfairfax/openinstallmedia/-/archive/1.1.5/openinstallmedia-1.1.5.tar.gz"
    sha256 "6ae396392b670b079f29234c468543c9857f600f037d6235c69118672b4c2085"
    license "GPL"
    version "1.1.5"
  
    def install
        bin.install "openinstallmedia.sh" => "openinstallmedia"
        bin.install "openinstallmacos.sh" => "openinstallmacos"
        bin.install "openprebootmedia.sh" => "openprebootmedia"
        bin.install "openrecoverymedia.sh" => "openrecoverymedia"
        bin.install "openupdatemacos.sh" => "openupdatemacos"
        prefix.install Dir["resources/*"]
    end
end