class MacosPatcher < Formula
    desc "macOS command line tool for running macOS on unsupported Macs"
    homepage "https://gitlab.com/julianfairfax/macos-patcher"
    url "https://gitlab.com/julianfairfax/macos-patcher/-/archive/3.6.1/macos-patcher-3.6.1.tar.gz"
    sha256 "980a5bcda8300c72eda19aa473a45ad9e54f619a50d10404ebfe474f90f5f3f3"
    license "GPL"
    version "3.6.1"
  
    def install
        bin.install "macOS Patcher.sh" => "macos-patcher"
        prefix.install Dir["resources/*"]
    end
end