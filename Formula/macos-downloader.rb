class MacosDownloader < Formula
    desc "macOS command line tool for downloading macOS installers and beta updates"
    homepage "https://gitlab.com/julianfairfax/macos-downloader"
    url "https://gitlab.com/julianfairfax/macos-downloader/-/archive/2.5.3/macos-downloader-2.5.3.tar.gz"
    sha256 "5b7f18e45e84cc3f5b390b5483d4c8ea4cb2de18600cabac84e23871e48672d7"
    license "GPL"
    version "2.5.3"
  
    def install
        bin.install "macOS Downloader.sh" => "macos-downloader"
        prefix.install Dir["resources/*"]
    end
end
