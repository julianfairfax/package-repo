#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="proton-mail-export"
version="$(wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/version.txt -O -)"
description="Proton Mail Export"

declare -a arches=("amd64")

for arch in ${arches[@]}; do
	install -d "$name"/usr/bin

	wget -q https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/"$arch"/proton-mail-export-cli -O "$name"/usr/bin/proton-mail-export-cli

	chmod +x "$name"/usr/bin/proton-mail-export-cli
	
	install -d "$name"/usr/lib
	
	wget -q https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/"$arch"/proton-mail-export.so -O "$name"/usr/lib/proton-mail-export.so

	mkdir "$name"/DEBIAN

	echo "Package: $name
Version: $(echo $version | sed 's/v//' | sed 's/-//g')
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee "$name"/DEBIAN/control

	dpkg-deb -Z xz -b "$name"/ .

	cp *.deb ../

	rm -rf "$name"/DEBIAN
done
