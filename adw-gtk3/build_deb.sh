#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="adw-gtk3"
version="$(wget https://api.github.com/repos/lassekongo83/adw-gtk3/releases/latest -O - -q | grep tag_name | sed 's/.* "//' | sed 's/".*//')"
description="The theme from libadwaita ported to GTK-3"

declare -a arches=("all")

wget -q https://github.com/lassekongo83/adw-gtk3/releases/download/$version/"$name"$version.tar.xz

install -d "$name"/usr/share/themes

tar -xf "$name"$version.tar.xz -C "$name"/usr/share/themes/

for arch in ${arches[@]}; do
	mkdir "$name"/DEBIAN

	echo "Package: $name
Version: $(echo $version | sed 's/v//')
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee "$name"/DEBIAN/control

	dpkg-deb -Z xz -b "$name"/ .

	cp *.deb ../

	rm -rf "$name"/DEBIAN
done
