#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="sbctl"
version="$(wget https://api.github.com/repos/Foxboron/sbctl/releases/latest -O - | grep tag_name | sed 's/.* "//' | sed 's/".*//')"
description="Secure Boot key manager"

declare -a arches=("amd64")

wget -q https://github.com/Foxboron/sbctl/releases/download/$version/"$name"-$version.tar.gz

tar -xf "$name"-$version.tar.gz

cd "$name"-$version

make

make PREFIX="../$name"/usr install

cd ../

for arch in ${arches[@]}; do
	mkdir "$name"/DEBIAN

	echo "Package: $name
Version: $version
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee "$name"/DEBIAN/control

	dpkg-deb -Z xz -b "$name"/ .

	cp *.deb ../

	rm -rf "$name"/DEBIAN
done